angular.module('pascalprecht.translate', ['ng'])

.run(['$translate', function ($translate) {

  var key = $translate.storageKey(),
      storage = $translate.storage();

  if (storage) {
    if (!storage.get(key)) {

      if (angular.isString($translate.preferredLanguage())) {
        // $translate.uses method will both set up and remember the language in case it's loaded successfully
        $translate.uses($translate.preferredLanguage());
      } else {
        storage.set(key, $translate.uses());
      }

    } else {
      $translate.uses(storage.get(key));
    }
  } else {
    if (angular.isString($translate.fallbackLanguage()) && $translate.fallbackLanguage() !== $translate.preferredLanguage()) {
      $translate.load($translate.fallbackLanguage());
    }
    if (angular.isString($translate.preferredLanguage())) {
      $translate.uses($translate.preferredLanguage());
    }
  }

}]);

angular.module('pascalprecht.translate').constant('$STORAGE_KEY', 'NG_TRANSLATE_LANG_KEY');

angular.module('pascalprecht.translate').factory('$translateDefaultInterpolation', ['$interpolate', function ($interpolate) {

  var $translateInterpolator = {},
      $locale,
      $identifier = 'default';

  $translateInterpolator.setLocale = function (locale) {
    $locale = locale;
  };

  $translateInterpolator.getInterpolationIdentifier = function () {
    return $identifier;
  };

  $translateInterpolator.interpolate = function (string, interpolateParams) {
    return $interpolate(string)(interpolateParams);
  };

  return $translateInterpolator;
}]);

angular.module('pascalprecht.translate').provider('$translate', ['$STORAGE_KEY', function ($STORAGE_KEY) {

  var $translationTable = {},
      $preferredLanguage,
      $fallbackLanguage,
      $uses,
      $nextLang,
      $storageFactory,
      $storageKey = $STORAGE_KEY,
      $storagePrefix,
      $missingTranslationHandlerFactory,
      $interpolationFactory,
      $interpolatorFactories = [],
      $loaderFactory,
      $loaderOptions,
      $notFoundIndicatorLeft,
      $notFoundIndicatorRight,
      NESTED_OBJECT_DELIMITER = '.';

  var translations = function (langKey, translationTable) {

    if (!langKey && !translationTable) {
      return $translationTable;
    }

    if (langKey && !translationTable) {
      if (angular.isString(langKey)) {
        return $translationTable[langKey];
      } else {
        angular.extend($translationTable, flatObject(langKey));
      }
    } else {
      if (!angular.isObject($translationTable[langKey])) {
        $translationTable[langKey] = {};
      }
      angular.extend($translationTable[langKey], flatObject(translationTable));
    }
  };

  var flatObject = function (data, path, result) {
    var key, keyWithPath, val;

    if (!path) {
      path = [];
    }
    if (!result) {
      result = {};
    }
    for (key in data) {
      if (!data.hasOwnProperty(key)) continue;
      val = data[key];
      if (angular.isObject(val)) {
        flatObject(val, path.concat(key), result);
      } else {
        keyWithPath = path.length ? ("" + path.join(NESTED_OBJECT_DELIMITER) + NESTED_OBJECT_DELIMITER + key) : key;
        result[keyWithPath] = val;
      }
    }
    return result;
  };

  this.translations = translations;

  this.addInterpolation = function (factory) {
    $interpolatorFactories.push(factory);
  };

  this.useMessageFormatInterpolation = function () {
    this.useInterpolation('$translateMessageFormatInterpolation');
  };

  this.useInterpolation = function (factory) {
    $interpolationFactory = factory;
  };

  this.preferredLanguage = function(langKey) {
    if (langKey) {
      $preferredLanguage = langKey;
    } else {
      return $preferredLanguage;
    }
  };

  this.translationNotFoundIndicator = function (indicator) {
    this.translationNotFoundIndicatorLeft(indicator);
    this.translationNotFoundIndicatorRight(indicator);
  };

  this.translationNotFoundIndicatorLeft = function (indicator) {
    if (!indicator) {
      return $notFoundIndicatorLeft;
    }
    $notFoundIndicatorLeft = indicator;
  };

  this.translationNotFoundIndicatorRight = function (indicator) {
    if (!indicator) {
      return $notFoundIndicatorRight;
    }
    $notFoundIndicatorRight = indicator;
  };

  this.fallbackLanguage = function(langKey) {
    if (langKey) {
      $fallbackLanguage = langKey;
    } else {
      return $fallbackLanguage;
    }
  };


  this.uses = function (langKey) {
    if (langKey) {
      if (!$translationTable[langKey] && (!$loaderFactory)) {
        // only throw an error, when not loading translation data asynchronously
        throw new Error("$translateProvider couldn't find translationTable for langKey: '" + langKey + "'");
      }
      $uses = langKey;
    } else {
      return $uses;
    }
  };

  var storageKey = function(key) {
    if (!key) {
      if ($storagePrefix) {
        return $storagePrefix + $storageKey;
      }
      return $storageKey;
    }
    $storageKey = key;
  };

  this.storageKey = storageKey;

  this.useUrlLoader = function (url) {
    this.useLoader('$translateUrlLoader', { url: url });
  };

  this.useStaticFilesLoader = function (options) {
    this.useLoader('$translateStaticFilesLoader', options);
  };

  this.useLoader = function (loaderFactory, options) {
    $loaderFactory = loaderFactory;
    $loaderOptions = options || {};
  };

  this.useLocalStorage = function () {
    this.useStorage('$translateLocalStorage');
  };

  /**
   * @ngdoc function
   * @name pascalprecht.translate.$translateProvider#useCookieStorage
   * @methodOf pascalprecht.translate.$translateProvider
   *
   * @description
   * Tells angular-translate to use `$translateCookieStorage` service as storage layer.
   *
   */
  this.useCookieStorage = function () {
    this.useStorage('$translateCookieStorage');
  };

  /**
   * @ngdoc function
   * @name pascalprecht.translate.$translateProvider#useStorage
   * @methodOf pascalprecht.translate.$translateProvider
   *
   * @description
   * Tells angular-translate to use custom service as storage layer.
   *
   */
  this.useStorage = function (storageFactory) {
    $storageFactory = storageFactory;
  };

  /**
   * @ngdoc function
   * @name pascalprecht.translate.$translateProvider#storagePrefix
   * @methodOf pascalprecht.translate.$translateProvider
   *
   * @description
   * Sets prefix for storage key.
   *
   * @param {string} prefix Storage key prefix
   */
  this.storagePrefix = function (prefix) {
    if (!prefix) {
      return prefix;
    }
    $storagePrefix = prefix;
  };

  /**
   * @ngdoc function
   * @name pascalprecht.translate.$translateProvider#useMissingTranslationHandlerLog
   * @methodOf pascalprecht.translate.$translateProvider
   *
   * @description
   * Tells angular-translate to use built-in log handler when trying to translate
   * a translation Id which doesn't exist.
   *
   * This is actually a shortcut method for `useMissingTranslationHandler()`.
   *
   */
  this.useMissingTranslationHandlerLog = function () {
    this.useMissingTranslationHandler('$translateMissingTranslationHandlerLog');
  };

  /**
   * @ngdoc function
   * @name pascalprecht.translate.$translateProvider#useMissingTranslationHandler
   * @methodOf pascalprecht.translate.$translateProvider
   *
   * @description
   * Expects a factory name which later gets instantiated with `$injector`.
   * This method can be used to tell angular-translate to use a custom
   * missingTranslationHandler. Just build a factory which returns a function
   * and expects a translation id as argument.
   *
   * Example:
   * <pre>
   *  app.config(function ($translateProvider) {
   *    $translateProvider.useMissingTranslationHandler('customHandler');
   *  });
   *
   *  app.factory('customHandler', function (dep1, dep2) {
   *    return function (translationId) {
   *      // something with translationId and dep1 and dep2
   *    };
   *  });
   * </pre>
   *
   * @param {string} factory Factory name
   */
  this.useMissingTranslationHandler = function (factory) {
    $missingTranslationHandlerFactory = factory;
  };

  /**
   * @ngdoc object
   * @name pascalprecht.translate.$translate
   * @requires $interpolate
   * @requires $log
   * @requires $rootScope
   * @requires $q
   *
   * @description
   * The `$translate` service is the actual core of angular-translate. It excepts a translation id
   * and optional interpolate parameters to translate contents.
   *
   * <pre>
   *  $scope.translatedText = $translate('HEADLINE_TEXT');
   * </pre>
   *
   * @param {string} translationId A token which represents a translation id
   * @param {object=} interpolateParams An object hash for dynamic values
   */
  this.$get = [
    '$log',
    '$injector',
    '$rootScope',
    '$q',
    function ($log, $injector, $rootScope, $q) {

    var Storage,
        defaultInterpolator = $injector.get($interpolationFactory || '$translateDefaultInterpolation');
        pendingLoader = false,
        interpolatorHashMap = {};

    if ($storageFactory) {
      Storage = $injector.get($storageFactory);

      if (!Storage.get || !Storage.set) {
        throw new Error('Couldn\'t use storage \'' + $storageFactory + '\', missing get() or set() method!');
      }
    }

    // if we have additional interpolations that were added via
    // $translateProvider.addInterpolation(), we have to map'em
    if ($interpolatorFactories.length > 0) {

      angular.forEach($interpolatorFactories, function (interpolatorFactory) {

        var interpolator = $injector.get(interpolatorFactory);
        // setting initial locale for each interpolation service
        interpolator.setLocale($preferredLanguage || $uses);
        // make'em recognizable through id
        interpolatorHashMap[interpolator.getInterpolationIdentifier()] = interpolator;
      });
    }

    var $translate = function (translationId, interpolateParams, interpolationId) {

      // determine translation table and current Interpolator
      var table = $uses ? $translationTable[$uses] : $translationTable,
          Interpolator = (interpolationId) ? interpolatorHashMap[interpolationId] : defaultInterpolator;

      // if the translation id exists, we can just interpolate it
      if (table && table.hasOwnProperty(translationId)) {
        return Interpolator.interpolate(table[translationId], interpolateParams);
      }

      // looks like the requested translation id doesn't exists.
      // Now, if there is a registered handler for missing translations and no
      // asyncLoader is pending, we execute the handler
      if ($missingTranslationHandlerFactory && !pendingLoader) {
        $injector.get($missingTranslationHandlerFactory)(translationId, $uses);
      }

      // since we couldn't translate the inital requested translation id,
      // we try it now with a fallback language, if a fallback language is
      // configured.
      if ($uses && $fallbackLanguage && $uses !== $fallbackLanguage){

        var translation = $translationTable[$fallbackLanguage][translationId];

        // check if a translation for the fallback language exists
        if (translation) {
          var returnVal;
          // temporarly letting Interpolator know we're using fallback language now.
          Interpolator.setLocale($fallbackLanguage);
          returnVal = Interpolator.interpolate(translation, interpolateParams);
          // after we've interpolated the translation, we reset Interpolator to proper locale.
          Interpolator.setLocale($uses);
          return returnVal;
        }
      }

      // applying notFoundIndicators
      if ($notFoundIndicatorLeft) {
        translationId = [$notFoundIndicatorLeft, translationId].join(' ');
      }

      if ($notFoundIndicatorRight) {
        translationId = [translationId, $notFoundIndicatorRight].join(' ');
      }

      return translationId;
    };

    /**
     * @ngdoc function
     * @name pascalprecht.translate.$translate#preferredLanguage
     * @methodOf pascalprecht.translate.$translate
     *
     * @description
     * Returns the language key for the preferred language.
     *
     * @return {string} preferred language key
     */
    $translate.preferredLanguage = function () {
      return $preferredLanguage;
    };
    /**
     * @ngdoc function
     * @name pascalprecht.translate.$translate#fallbackLanguage
     * @methodOf pascalprecht.translate.$translate
     *
     * @description
     * Returns the language key for the fallback language.
     *
     * @return {string} fallback language key
     */
    $translate.fallbackLanguage = function () {
      return $fallbackLanguage;
    };

    /**
     * @ngdoc function
     * @name pascalprecht.translate.$translate#proposedLanguage
     * @methodOf pascalprecht.translate.$translate
     *
     * @description
     * Returns the language key of language that is currently loaded asynchronously.
     *
     * @return {string} language key
     */
    $translate.proposedLanguage = function () {
      return $nextLang;
    };

    /**
     * @ngdoc function
     * @name pascalprecht.translate.$translate#storage
     * @methodOf pascalprecht.translate.$translate
     *
     * @description
     * Returns registered storage.
     *
     * @return {object} Storage
     */
    $translate.storage = function () {
      return Storage;
    };

    /**
     * @ngdoc function
     * @name pascalprecht.translate.$translate#uses
     * @methodOf pascalprecht.translate.$translate
     *
     * @description
     * Tells angular-translate which language to uses by given language key. This method is
     * used to change language at runtime. It also takes care of storing the language
     * key in a configured store to let your app remember the choosed language.
     *
     * When trying to 'use' a language which isn't available it tries to load it
     * asynchronously with registered loaders.
     *
     * Returns promise object with loaded language file data
     * @example
     * $translate.uses("en_US").then(function(data){
     *  $scope.text = $translate("HELLO");
     * });
     *
     * @param {string} key Language key
     * @return {string} Language key
     */
    $translate.uses = function (key) {

      if (!key) {
        return $uses;
      }

      var success = function () {
        $uses = key;

        if ($storageFactory) {
          Storage.set($translate.storageKey(), $uses);
        }

        // inform default interpolator
        defaultInterpolator.setLocale($uses);
        // inform all others to!
        angular.forEach(interpolatorHashMap, function (interpolator, id) {
          interpolatorHashMap[id].setLocale($uses);
        });

        $rootScope.$broadcast('translationChangeSuccess');
      };

      var error = function () {
        $rootScope.$broadcast('translationChangeError');
      };

      return $translate.load(key, success, error);
    };

    /**
     * @ngdoc function
     * @name pascalprecht.translate.$translate#load
     * @methodOf pascalprecht.translate.$translate
     *
     * @description
     * Tells angular-translate to load a language asynchronously by given language key.
     * Optionnal callbacks can be used for success/error specific tasks.
     *
     * @param {string} key Language key
     * @param {function=} success Optionnal callback on loading success
     * @param {function=} error Optionnal callback on loading error
     * @return {string} Language key
     */
    $translate.load = function (key, success, error) {

      if (!key) {
        throw "No language key specified for loading.";
      }

      var deferred = $q.defer();

      if (!$translationTable[key]) {

        pendingLoader = true;
        $nextLang = key;

        $injector.get($loaderFactory)(angular.extend($loaderOptions, {
          key: key
        })).then(function (data) {

          var translationTable = {};

          if (angular.isArray(data)) {
            angular.forEach(data, function (table) {
              angular.extend(translationTable, table);
            });
          } else {
            angular.extend(translationTable, data);
          }

          translations(key, translationTable);

          if (angular.isFunction(success)) {
            success();
          }

          pendingLoader = false;
          deferred.resolve(key);
        }, function (key) {
          if (angular.isFunction(error)) {
            error();
          }
          deferred.reject(key);
        });

        return deferred.promise;
      }

      if (angular.isFunction(success)) {
        success();
      }

      deferred.resolve(key);
      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name pascalprecht.translate.$translate#storageKey
     * @methodOf pascalprecht.translate.$translate
     *
     * @description
     * Returns the key for the storage.
     *
     * @return {string} storage key
     */
    $translate.storageKey = function() {
      return storageKey();
    };

    // If at least one async loader is defined and there are no (default) translations available
    // we should try to load them.
    if ($loaderFactory && angular.equals($translationTable, {})) {
      $translate.uses($translate.uses());
    }

    return $translate;
  }];
}]);
