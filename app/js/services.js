'use strict';

ecwidShowcase.factory('storeId', function() {
    return {
        value: '2900003'
    }
});

ecwidShowcase.factory('storeAPI', function($http, $q, storeId) {
    return {
        getAPIResponse: function(apiMethod, parameterName, parameterValue) {
            var deferred = $q.defer();
            var apiUrl = 'http://appdev.ecwid.com/api/v1/'+storeId.value+
            '/'+apiMethod+'?'+parameterName+'='+parameterValue+'&callback=JSON_CALLBACK';
            
            if (typeof parameterName === 'undefined') {
                apiUrl = 'http://appdev.ecwid.com/api/v1/'+storeId.value+
            '/'+apiMethod+'?callback=JSON_CALLBACK';
            }
            
            $http({method: 'JSONP', url: apiUrl})
            .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
            .error(function(data, status, headers, config) {
                    deferred.reject(status);
                });
                
            return deferred.promise;
        }
    }
});

ecwidShowcase.factory('storeData', function(storeAPI, $rootScope) {
    return {
        getCategoryList: function(parentId) {
            return storeAPI.getAPIResponse('categories', 'parent', parentId);
        },
        getCategoryById: function(categoryId) {
            return storeAPI.getAPIResponse('category', 'id', categoryId);
        },
        getProductsListByCategory: function(categoryId) {
            return storeAPI.getAPIResponse('products', 'category', categoryId);
        },
        getProductById: function(productId) {
            return storeAPI.getAPIResponse('product', 'id', productId);
        },
        getStoreProfile: function() {
            if (typeof $rootScope.storeProfile == 'undefined') {
                $rootScope.storeProfile = storeAPI.getAPIResponse('profile');
                $rootScope.storeProfile.then(
                    function(data) {
                        $rootScope.storeProfile = data;
                    },
                    function(status) {
                        
                    });
            }
            return $rootScope.storeProfile;
        }
    }
});

ecwidShowcase.factory('headerData', function(storeData) {
    return {
        load: function($scope, category) {
            function getCategoryParents(category) {
                if (typeof category.parentId === 'undefined') {
                    return [category];
                }
                
                var parent = storeData.getCategoryById(category.parentId);
                parent.then(function(data) {
                        parent = data;
                    },
                    function(status) {
                        return [];
                    });
                var parents = getCategoryParents(parent);
                parents.push(category);
                return parents;
            }
            
            if (category != '0') {
                $scope.currentCategory = storeData.getCategoryById(category);
                $scope.currentCategory.then(function(data) {
                        $scope.currentCategory = data;
                        $scope.parentCategories = getCategoryParents(data);
                    },
                    function(status) {
                    })
            }
            $scope.rootCategories = storeData.getCategoryList(0);
            return $scope;
        }
    }
});

ecwidShowcase.factory('productOperations', function() {
    return {
        //check products with all options values for equality
        equals: function(a, b) {
            if (a.id !== b.id) {
                return false;
            }
            var found = true;
            for (var optionIdx = 0; optionIdx < a.options.length; ++optionIdx) {
                if (a.options[optionIdx].type != 'CHECKBOX' && a.options[optionIdx].value !== b.options[optionIdx].value) {
                    return false;
                }
                else if (a.options[optionIdx].type == 'CHECKBOX') {
                    for (var choiceIdx = 0; choiceIdx < a.options[optionIdx].choices.length; ++choiceIdx) {
                        if (a.options[optionIdx].choices[choiceIdx].selected != b.options[optionIdx].choices[choiceIdx].selected) {
                            return false;
                        }
                    }
                }
            }
            return true;
        },
        //convert options array to representable string
        optionsToString: function(product) {
            var res = [];
            for (var i = 0; i < product.options.length; ++i) {
                if (product.options[i].type != 'CHECKBOX') {
                    if (typeof product.options[i].value != 'undefined') {
                        res.push(product.options[i].name + ': ' + product.options[i].value);
                    }
                }
                else {
                    var checkBoxSelected = '';
                    for (var j = 0; j < product.options[i].choices.length; ++j) {
                        if (product.options[i].choices[j].selected) {
                            if (checkBoxSelected.length > 0) {
                                checkBoxSelected += ', ';
                            }
                            checkBoxSelected += product.options[i].choices[j].text;
                        }
                    }
                    if (checkBoxSelected.length > 0) {
                        res.push(product.options[i].name + ': ' + checkBoxSelected);
                    }
                }
            }
            return res.join(', ');
        }
    }
});

ecwidShowcase.factory('cartStorage', function(localStorageService, productOperations, $cookieStore) {
    return {
        getProducts: function() {
            var productsString = localStorageService.get('cart');
            var products = angular.fromJson(productsString);
            if (products == null) {
                products = [];
            }
            return products;
        },
        getProductCountFromCart: function(product) {
            var productsString = localStorageService.get('cart');
            var products = angular.fromJson(productsString);
            if (products == null) {
                products = [];
            }
            
            var index;
            for (index = 0; index < products.length; ++index) {
                if (productOperations.equals(products[index], product)) {
                    return products[index].ordered;
                }
            }
            return 0;
        },
        addProduct: function(product, count) {
            var productsString = localStorageService.get('cart');
            var products = angular.fromJson(productsString);
            
            if (products == null) {
                products = [];
            }
            
            var productInCart = false;
            for (var index = 0; index < products.length; ++index) {
                if (productOperations.equals(products[index], product)) {
                    products[index].ordered += count;
                    productInCart = true;
                    break;
                }
            }
            
            if (!productInCart) {
                var newProduct = {ordered: count, id: product.id, thumbnailUrl: product.thumbnailUrl,
                    options: product.options, price:product.price, name: product.name, quantity: product.quantity};
                products.push(newProduct);
            }
            
            localStorageService.add('cart', angular.toJson(products));
        },
        removeProduct: function(product) {
            var productsString = localStorageService.get('cart');
            var products = angular.fromJson(productsString);

            for (var index = 0; index < products.length; ++index) {
                if (productOperations.equals(products[index], product)) {
                    products.splice(index, 1);
                    break;
                }
            }
            
            localStorageService.add('cart', angular.toJson(products));
        },
        clearCart: function() {
            localStorageService.remove('cart');
        }
    }
});
