'use strict';

angular.module('ecwidShowcase.controllers', [])
    .controller('GoodsListController',  function($scope, $routeParams, $location, cartStorage, storeData, headerData) {
		
        $scope.params = $routeParams;
        $scope.categories = storeData.getCategoryList($scope.params.category);
        $scope.products = storeData.getProductsListByCategory($scope.params.category);
        $scope.cartContents = cartStorage.getProducts();
        $scope = headerData.load($scope, $routeParams.category);
        $scope.predicate = '';
        
        $scope.products.then(function(data) {
            $scope.products = data;
            for (var i = 0; i < $scope.products.length; ++i) {
                $scope.products[i].inCart = false;
                for (var j = 0; j < $scope.cartContents.length; ++j) {
                    if ($scope.products[i].id == $scope.cartContents[j].id) {
                        $scope.products[i].inCart = true;
                    }
                }
            }
        },
        function(status) {
            $location.url('/list/0');
        });
    })
    .controller('ProductController', function($scope, $routeParams, $location, cartStorage, storeData, headerData) {
        var getDefaultCategory = function(product) {
            var index;
            for (index = 0; index < product.categories.length; ++index) {
                if (product.categories[index].defaultCategory) {
                    return product.categories[index].id;
                }
            }
        };
        
        var getMaximumAvailableCount = function(product) {
            if (typeof product.quantity !== 'undefined') {
                return product.quantity;
            }
            return 9007199254740992;
        }
        
        $scope.addToCart = function() {
            if ($scope.product.quantity == null || $scope.countForAdding <= $scope.product.quantity - $scope.getAlreadyInCart($scope.product)) {
                var count = $scope.countForAdding;
                cartStorage.addProduct($scope.product, count); 
                $scope.maximumAvailableCount = getMaximumAvailableCount($scope.product) - cartStorage.getProductCountFromCart($scope.product);           
                $scope.countForAdding = 1;
                $scope.everythingInCart = getMaximumAvailableCount($scope.product) <= cartStorage.getProductCountFromCart($scope.product);
            }
        }
        
        $scope.getAlreadyInCart = function(product) {
            return cartStorage.getProductCountFromCart($scope.product);
        }
        
        
        $scope.params = $routeParams;
        $scope.product = storeData.getProductById($scope.params.productId);
        $scope.product.then(function(data) {
                $scope.product = data;
                $scope = headerData.load($scope, getDefaultCategory($scope.product));
                
                $scope.maximumAvailableCount = getMaximumAvailableCount($scope.product) - cartStorage.getProductCountFromCart($scope.product);
                $scope.countForAdding = 1;
                $scope.everythingInCart = getMaximumAvailableCount($scope.product) <= cartStorage.getProductCountFromCart($scope.product);

                for (var i = 0; i < $scope.product.options.length; ++i) {
                    var option = $scope.product.options[i];
                    if (typeof option.defaultChoice !== 'undefined') {
                        option.value = option.choices[option.defaultChoice].text;
                    }
                    if (option.type == 'CHECKBOX') {
                        for (var j = 0; j < option.choices.length; ++j) {
                            option.choices[j].selected = false;
                        }
                    }
                }
            },
            function(status) {
                $location.url('/list/0');
            });
    })
    .controller('CartController', function($scope, cartStorage, headerData, productOperations) {
        var updateCartData = function() {
            $scope.cartContents = cartStorage.getProducts();
            $scope.calculatePrices($scope.cartContents);
            
            for (var i = 0; i < $scope.cartContents.length; ++i) {
                $scope.cartContents[i].optionsText = productOperations.optionsToString($scope.cartContents[i]);
            }
        }
        
        $scope.saveCartData = function() {
            var enoughProduct = true;
            var length = $scope.cartContents.length;
            for (var i = 0; i < length; ++i) {
                if ($scope.cartContents[i].quantity < $scope.cartContents[i].ordered || ($scope.cartContents[i].ordered == null)) {
                    enoughProduct = false;
                    break;
                }
            }
            
            if (enoughProduct) {
                for (var i = 0; i < length; ++i) {
                    if ($scope.cartContents[i].ordered < 1) {
                        cartStorage.removeProduct($scope.cartContents[i]);
                        $scope.cartContents.splice(i, 1);
                        --i;
                        --length;
                    }
                }
                cartStorage.clearCart();
                for (var i = 0; i < $scope.cartContents.length; ++i) {
                    cartStorage.addProduct($scope.cartContents[i], $scope.cartContents[i].ordered);
                }
            }
        }
        
        $scope.calculatePrices = function(cart) {
            $scope.totalPrice = 0;
            for (var i = 0; i < cart.length; ++i) {
                var addition = 0;
                for (var j = 0; j < cart[i].options.length; ++j) {
                    var option = cart[i].options[j];
                    var optionType = option.type;
                    if (optionType == 'SELECT' || optionType == 'RADIO' || optionType == 'CHECKBOX') {
                        //seems like price should be set as for default choice but who knows
                        //treat default choice like any other
                        var selected = [];
                        for (var k = 0; k < option.choices.length; ++k) {
                            if (option.choices[k].selected || option.choices[k].text == option.value) {
                                selected.push(k);
                            }
                        }
                        if (selected.length == 0 && typeof option.defaultChoice !== 'undefined') {
                            selected.push(option.defaultChoice);
                        }
                        
                        for (var k = 0; k < selected.length; ++k) {
                            if (option.choices[selected[k]].priceModifierType == 'ABSOLUTE') {
                                addition += option.choices[selected[k]].priceModifier;
                            }
                            else if (option.choices[selected[k]].priceModifierType == 'PERCENT') {
                                addition += option.choices[selected[k]].priceModifier/100.0*cart[i].price;
                            }
                            else {
                                //should not happen
                                console.warn('API error. Unknown priceModifierType.');
                            }
                        }
                    }
                }
                cart[i].addition = addition;
                cart[i].totalPrice = (cart[i].price + addition) * cart[i].ordered;
                $scope.totalPrice += cart[i].totalPrice;
            }
        };
        
        
        $scope.clearCart = function() {
            cartStorage.clearCart();
            updateCartData();
        }
        
        $scope = headerData.load($scope, 0);
        updateCartData();
    });
