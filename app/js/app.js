'use strict';


var ecwidShowcase = angular.module('ecwidShowcase', ['ecwidShowcase.controllers', 'ecwidShowcase.filters', 
                                    'pascalprecht.translate', 'LocalStorageModule', 'ngCookies']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/list/:category', {templateUrl: 'templates/list.html', controller: 'GoodsListController'});
    $routeProvider.when('/product/:productId', {templateUrl: 'templates/product.html', controller: 'ProductController'});
    $routeProvider.when('/cart', {templateUrl: 'templates/cart.html', controller: 'CartController'});
    $routeProvider.otherwise({redirectTo: '/list/0'});
    
  }]);

ecwidShowcase.config(['$translateProvider', function ($translateProvider) {
    $translateProvider.translations('en', {
    'STORETITLE': 'Shop',
    'CARTTITLE': 'Cart',
    });
     
    $translateProvider.translations('ru', {
        'STORETITLE': 'Магазин',
        'CARTTITLE': 'Корзина',
        'SORTORDER': 'Сортировать по',
        'NAMEASC': 'Названию от А до Я',
        'SKUTITLE': 'Артикул',
        'NAMEDESC': 'Названию от Я до А',
        'PRICEASC': 'Цене от низкой к высокой',
        'PRICEDESC': 'Цене от высокой к низкой',
        'DATEASC': 'Дате',
        'ADDTOCART': 'В корзину',
        'COUNT': 'Кол-во',
        'CLEARCART': 'Очистить корзину',
        'SAVECART': 'Сохранить изменения',
        'SUBTOTAL': 'Итого',
        'AVAILABLE': 'доступно',
        'PRICETITLE': 'Цена',
        'PRODUCTTITLE': 'Товар',
        'TOREMOVESETZERO': 'Чтобы удалить предмет из корзины, выставьте его количество в 0',
    });
     
    $translateProvider.preferredLanguage('ru');
    $translateProvider.fallbackLanguage('en');
}]);

