'use strict';

angular.module('ecwidShowcase.filters', [])
.filter('ecwidCurrency', function (storeData) {
    return function(number) {
        var storeProfile = storeData.getStoreProfile();
        return (storeProfile.currencyPrefix || '') + 
            accounting.formatMoney(number, storeProfile.currencySuffix, storeProfile.numericPrecision, 
                storeProfile.groupSeparator, storeProfile.decimalSeparator, '%v%s');
        //return accounting.formatMoney(number);
    };
});

angular.module('pascalprecht.translate')
.filter('translate', ['$parse', '$translate', function ($parse, $translate) {
  return function (translationId, interpolateParams, interpolation) {

    if (!angular.isObject(interpolateParams)) {
      interpolateParams = $parse(interpolateParams)();
    }
    return $translate(translationId, interpolateParams, interpolation);
  };
}]);
